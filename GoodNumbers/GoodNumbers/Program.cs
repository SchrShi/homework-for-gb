﻿//6. *Написать программу подсчета количества «хороших» чисел в диапазоне от 1 до 1 000 000 000. «Хорошим» называется число, которое делится на сумму своих цифр.
//Реализовать подсчёт времени выполнения программы, используя структуру DateTime.
//
//Домашнее задание выполнил Артем Семилетов https://geekbrains.ru/users/3804568

using System;

namespace GoodNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime before = DateTime.Now; // Засекаем время начала работы
            int quantityForFunc = 0;
            int quantityForString = 0;
            int number = 1;
            while (number < 1000)
            {
                if (IntToStringSum(number)) quantityForString++;
                if (Func(number)) quantityForFunc++;
                number++; // Следующее число
            }
            Console.WriteLine($"Кол-во найденных хороших чисел: {quantityForFunc} для Func и {quantityForString} для IntToString; \nВремя работы программы = {DateTime.Now - before}");
        }

        static bool Func(int num)
        {
            int sumCount = 0;
            int forNum = num;
            for (int i = 0; i <= (num.ToString()).Length; i++)
            {
                sumCount += forNum % 10;
                forNum /= 10;
            }
            if (num % sumCount == 0) return true; //Если остаток от деления числа на его сумму равен 0, то прибавляем
            else return false;
        }

        //Специально для Salad - треш, угар и содомия.
        //Реализация суммирования цифр числа методом перебора строки.
        static bool IntToStringSum(int num)
        {
            int sumCount = 0;
            string count = num.ToString();
            foreach(char element in count)
            {
                sumCount += (int)Char.GetNumericValue(element);
            }
            if (num % sumCount == 0) return true;
            else return false;

        }

    }
}
