﻿//7. a) Разработать рекурсивный метод, который выводит на экран числа от a до b(a<b).
//б) * Разработать рекурсивный метод, который считает сумму чисел от a до b.
//
//Домашнее задание выполнил Артем Семилетов https://geekbrains.ru/users/3804568

using System;

namespace Recursion
{
    class Program
    {
        private static int sum;
        static void Main(string[] args)
        {
            Console.WriteLine("Введите число a");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите число b");
            int b = Convert.ToInt32(Console.ReadLine());
            Func(a, b);
            Console.WriteLine(sum);
        }

        static void Func(int a, int b)
        {
            if (a<b)
            {
                sum += a;
                Console.WriteLine(a);
                a++;
                Func(a, b);
            }
        }

    }
}
