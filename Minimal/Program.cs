﻿//1. Написать метод, возвращающий минимальное из трёх чисел.
//
//Домашнее задание выполнил Артем Семилетов https://geekbrains.ru/users/3804568

using System;

namespace Minimal
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите первое число:");
            int first = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите второе число:");
            int second = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите третье число:");
            int three = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ваше наименьшее число - " + Minimal(first,second,three));
        }

        static int Minimal(int first, int second, int three)
        {
            if (first < second && first < three) return first;
            else if (second < first && second < three) return second;
            else return three;
        }

    }
}
