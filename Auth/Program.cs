﻿//4. Реализовать метод проверки логина и пароля. На вход метода подается логин и пароль. На выходе истина, если прошел авторизацию, и ложь, если не прошел (Логин: root, Password: GeekBrains).
//Используя метод проверки логина и пароля, написать программу: пользователь вводит логин и пароль, программа пропускает его дальше или не пропускает. 
//С помощью цикла do while ограничить ввод пароля тремя попытками.
//
//Домашнее задание выполнил Артем Семилетов https://geekbrains.ru/users/3804568

using System;

namespace Auth
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 3; //Кол-во попыток для входа.
            do
            {
                i--;
                Console.Write("Введите логин: ");
                string currentLogin = Console.ReadLine();
                Console.Write("Введите пароль: ");
                string currentPassword = Console.ReadLine();
                if (Verificaton(currentLogin, currentPassword)) { Console.WriteLine("Вы успешно вошли"); break; } //Если верно, пишем об этом и завершаем работу
                else if (i > 0) Console.WriteLine($"Неверный логин или пароль. У вас осталось {i} попыток");
                else Console.WriteLine("К сожалению, ваше кол-во попыток закончилось.");
            } while (i > 0);
        }

        static bool Verificaton(string login, string password)
        {
            if (login == "root" && password == "GeekBrains") return true;
            else return false;
        }

    }
}
