﻿//5. а) Написать программу, которая запрашивает массу и рост человека, вычисляет его индекс массы и сообщает, нужно ли человеку похудеть, набрать вес или всё в норме.
//б) * Рассчитать, на сколько кг похудеть или сколько кг набрать для нормализации веса.
//
//Домашнее задание выполнил Артем Семилетов https://geekbrains.ru/users/3804568
using System;

namespace IMT
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите ваш рост в сантиметрах:");
            int currentHeight = Convert.ToInt32(Console.ReadLine());
            Console.Write("Введите ваш вес в килограммах:");
            int currentWeight = Convert.ToInt32(Console.ReadLine());
            IMT(currentHeight, currentWeight);
        }

        static void IMT(float height, float weight)
        {
            height /= 100;
            float imt = weight / (height * height);
            if (imt <= 18.5f)
            {
                Console.WriteLine($"Дефицит массы тела, Ваш ИМТ составляет {imt}.\nДля нормы, по индексу Ноордена, вам требуется набрать {(height*100 * 0.42f) - weight} кг");
            }
            else if (imt >= 25)
            {
                Console.WriteLine($"Ожирение тела, Ваш ИМТ составляет {imt}.\nДля нормы, по индексу Ноордена, вам требуется скинуть {weight - (height*100 * 0.42f)} кг");
            }
            else Console.WriteLine($"Ваш ИМТ составляет {imt}. Нормальный индекс тела.");
        }
    }
}
