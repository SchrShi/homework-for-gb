﻿//2. Написать метод подсчета количества цифр числа.
//
//Домашнее задание выполнил Артем Семилетов https://geekbrains.ru/users/3804568

using System;

namespace NumberCount
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите число: ");
            int number = Convert.ToInt32(Console.ReadLine()); //Принимаю только int, потому что тз без дробных чисел.
            Console.WriteLine("Ваше кол-во чисел: " + NumberCount(number));
        }

        static int NumberCount(int number)
        {
            if (number < 0) number *= -1; // если меньше 0, то убираю минус;
            return (Convert.ToString(number)).Length; //Отдаю
        }

    }
}
