﻿//3. С клавиатуры вводятся числа, пока не будет введен 0. Подсчитать сумму всех нечетных положительных чисел.
//
//Домашнее задание выполнил Артем Семилетов https://geekbrains.ru/users/3804568

using System;

namespace SumNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;
            int n = 2; //Просто затычка, чтобы пропустил цикл
            while (n != 0)
            {
                Console.Write("Введите число: ");
                n = Convert.ToInt32(Console.ReadLine());
                if (n > 0 && n % 2 > 0) sum += n;
            }
            Console.WriteLine($"Сумма нечетных положительных чисел: {sum}");
        }
    }
}
